""""""""""""""""""""""""""""""""""""""""""
set exrc
set nocompatible
filetype plugin on
filetype off

" *** PLUGINS LIST ***
" single quotes ONLY!
call plug#begin()

" general
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/limelight.vim'
Plug 'junegunn/vim-slash' " enhanced searching
Plug 'kien/ctrlp.vim'
Plug 'mileszs/ack.vim'
Plug 'scrooloose/nerdtree'
" Plug 'scrooloose/syntastic'
Plug 'Shougo/vimproc.vim'
Plug 'Shougo/vimproc.vim'
Plug 'Shougo/vimshell.vim'
Plug 'Shougo/vimshell.vim'
Plug 'tomtom/tcomment_vim'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-fugitive' | Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive' | Plug 'gregsexton/gitv', { 'on': 'Gitv' }
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-speeddating' " inc/dec datetimes
Plug 'Valloric/YouCompleteMe', {'do': 'python install.py --clang-completer --go-completer --rust-completer' }
Plug 'vim-scripts/vimwiki'
Plug 'mhinz/vim-startify'

" themes
Plug 'jacoborus/tender.vim'
Plug 'vim-scripts/summerfruit256.vim'
Plug 'mhartington/oceanic-next'
Plug 'KeitaNakamura/neodark.vim'
Plug 'ajmwagar/vim-deus'
Plug 'tyrannicaltoucan/vim-quantum'
Plug 'raphamorim/lucario'
Plug 'joshdick/onedark.vim'
Plug 'morhetz/gruvbox'
Plug 'tomasr/molokai'
Plug 'chriskempson/vim-tomorrow-theme'

" writing
Plug 'junegunn/goyo.vim', { 'for': 'markdown' }
Plug 'junegunn/vim-journal'

" cpp
Plug 'octol/vim-cpp-enhanced-highlight', { 'for': 'cpp' }

" docker
Plug 'docker/docker'

" elixir
Plug 'elixir-lang/vim-elixir'

" git
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-git'

" go
Plug 'fatih/vim-go'

" haskell
Plug 'eagletmt/ghcmod-vim', { 'for': 'haskell'}

" html
Plug 'mattn/emmet-vim', { 'for': 'html' }

" javascript
Plug 'pangloss/vim-javascript', { 'for': 'javascript' }

" rust
Plug 'rust-lang/rust.vim', { 'for': 'rust' }

call plug#end()

" *** PLUGINS SETTINGS ***

" vim-airline
let g:airline_powerline_fonts = 1
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.space = ' '
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '#'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_theme = 'tender'

" CtrlP
let g:ctrlp_match_window= 'max:30'

" cpp-extended-highlight
let g:cpp_class_scope_highlight = 1
let g:cpp_experimental_template_highlight = 1

" syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_sh_checkers = ["shellcheck"]
let g:syntastic_sh_shellcheck_args = "-s bash"
let g:syntastic_enable_cpp_checker = 0
let g:syntastic_enable_c_checker = 0

" YouCompleteMe
let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_auto_trigger = 1
let g:ycm_confirm_extra_conf = 0


" *** VISUAL SETTINGS ***
set encoding=utf-8
set t_Co=256         " extended color set
syntax on            " syntax highlighting
colorscheme neodark
set background=dark

" *** OPTIONS FOR GUI ***
set go-=m "remove menu bar
set go-=T "remove toolbar
set go-=r "remove right scroll bar
set go-=L "remove left scroll bar


" *** GENERAL SETTINGS ***
set autoindent
set autoread
set backspace=2 " 2
set cursorline
set number           " line numbers
set ruler            " show cursor position
set expandtab       " replace tabs with spaces
set ignorecase
set incsearch
set laststatus=2
set matchpairs+=<:> " make < and > match
set mouse=a
set mousehide       " hide on typing
set nocompatible
set nolazyredraw
set nowb
set nowrap
set ruler
set shiftround      " round indent to multiple of shiftwidth
set shiftwidth=4
set shortmess=atI
"set showmatch       " show matching brackets
set showmode
set smartcase
set smartindent
set smarttab        " after hitting tab, indent line according to shiftwidth
set so=5 " lines under cursor
set softtabstop=4
set tabstop=4
set termencoding=utf-8
set textwidth=79
set ttyfast         " indicates fast terminal connection
set virtualedit=block
set wildignore='.svn,*.o,*.d,*.a,*.gcno,*.gcda'
set wildmenu
set wildmode=list:longest

set tags=./tags;/   " ctags file

" *** KEY MAPPING ***

" moving between buffers
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-h> <C-W>h
nnoremap <C-l> <C-W>l

" disable arrows
" noremap <Up> <nop>
" noremap <Down> <nop>
" noremap <Left> <nop>
" noremap <Right> <nop>
" inoremap <Up> <nop>
" inoremap <Down> <nop>
" inoremap <Left> <nop>
" inoremap <Right> <nop>

" tabs 
nnoremap <C-t> :tabnew<cr>
nnoremap <C-e> :tabclose<cr>

" CtrlP
nnoremap <leader>b :CtrlPBuffer<cr>

" Limelight
nnoremap <leader>ll :Limelight!! 0.7<cr>

" NERDTree
nnoremap <leader>n :NERDTreeToggle<cr>

" vim-fugitive
nnoremap <leader>gc :Gcommit<cr>
nnoremap <leader>gl :Gblame<cr>
nnoremap <leader>gr :Gbrowse<cr>
nnoremap <leader>gs :Gstatus<cr>

" VimShell
nnoremap <leader>h :VimShell<cr>

" YouCompleteMe
nnoremap ,gl :YcmCompleter GoToDeclaration<CR>
nnoremap ,gf :YcmCompleter GoToDefinition<CR>
nnoremap ,gg :YcmCompleter GoToDefinitionElseDeclaration<CR>

"Other bindings
nnoremap <leader><space> :e!<cr>

" *** OTHER ***
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

if has("autocmd")
    autocmd Filetype gitcommit setlocal spell textwidth=72
endif

" Properly display man pages
" ==========================
runtime ftplugin/man.vim
if has("gui_running")
	nnoremap K :<C-U>exe "Man" v:count "<C-R><C-W>"<CR>
endif

set secure
