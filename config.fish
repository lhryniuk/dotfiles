set fish_greeting

set -x -U GOPATH $HOME/data/code/go
set -x PATH $GOPATH/bin $HOME/.local/bin /usr/local/bin /usr/sbin $PATH
set -x FZF_DEFAULT_OPTS "--bind='ctrl-o:execute(code {})+abort'"

source $HOME/.cargo/env

alias e "vim (fzf)"
alias cat "bat"
alias preview "fzf --preview 'bat --color \"always\" {}'"
alias wat "tldr"
